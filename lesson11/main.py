# pip install openpyxl

import openpyxl
import math

file_name = "source.xlsx"

def func(x):
    if not isinstance(x, (int, float)) :
        raise TypeError("Argument must be number")
    if x <= -10:   
        return -5
    if x >= 10:
        try:
            result = math.exp(x/5)
        except OverflowError:
            result = math.inf
        return result
    
    if x == 0:
        return 2
    else:
        return 1/x


# Підгружаємо workbook (wb) і обираємо активний worksheet (ws)
wb = openpyxl.load_workbook(file_name)
ws = wb.active

# Робимо розрахунок і додаємо результат у файл
for i in range(1,ws.max_row) :
    x = ws.cell(row=i, column=1).value
    if isinstance(x, (int, float)) :
        ws.cell(row=i, column=2, value=func(x))

# зберігаємо workbook
wb.save(file_name)