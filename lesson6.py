from geopy.distance import geodesic

# Координати міст
coords = {
    'Kyiv': (50.4501, 30.5234),
    'Sumy': (50.9216, 34.8003),
    'Rivne': (50.6231, 26.2274),
    'Odesa': (46.4775, 30.73262),
    'Dnipro': (48.4500, 34.9833),
    'Lviv': (49.8383, 24.0232),
    'Kharkiv': (50.0000, 36.2500),
    'Chernigiv': (51.5055, 31.2849),
    'Zytomyr': (50.2648, 28.6867),
    'Poltava': (49.5937, 34.5407)
}

# Розрахунок відстані між двома містами
def calculate_distance(coord1, coord2):
    return geodesic(coord1, coord2).kilometers

 # Функція для перебору пар міст та виведення результатів
def calculate_and_print_distances(coords):
    city_list = list(coords.keys())
    num_cities = len(city_list)

    count = 0
    for i in range(num_cities):
        for j in range(num_cities):
            if i == j:
                continue

            count += 1
            city1, city2 = city_list[i], city_list[j]
            distance = calculate_distance(coords[city1], coords[city2])

            print(f'{count}: Відстань між {city1} та {city2}: {distance:.2f} км')

# Виклик функції для розрахунку та виведення відстаней
calculate_and_print_distances(coords)