from ticket import Ticket
from route import Route
from bus import Bus

class Controller:
    def __init__(self):
        self.routes = [
            Route(1, "R01", "Kyiv-Sumy", "Kyiv", "Sumy", 335),
            Route(2, "R02", "Sumy-Dnipro", "Sumy", "Dnipro", 380)
        ]

        self.buses = [
            Bus(11, "K01", "BM1213CA", 50),
            Bus(12, "S01","KA1512PC", 35),
            Bus(13, "K02","CB6755PC", 30),
        ]

        self.tickets = [
            Ticket(1, 11, "Kozak I", "9", "A12", "10:00 AM", "Sumy"),
            Ticket(1, 11, "Bulava S", "9", "B5", "10:00 AM", "Sumy"),
            Ticket(2, 12, "Strashko O", "14", "A4", "10:30 AM", "Dnipro"),
            Ticket(2, 12, "Peremoga V", "14", "A5", "10:30 AM", "Dnipro"),
            Ticket(1, 11, "Kovil A", "9", "A3", "10.00 AM", "Romny"),
            Ticket(1, 13, "Sonce D", "3", "B2", "18.00 AM", "Sumy")
        ]

    def display_routes(self):
        for route in self.routes:
            print(f"Route Number: {route.route_number}, Route Name: {route.route_name}, Start Point: {route.start_point}, "+
            f"Finish Point: {route.end_point}, Distance: {route.distance}") 

    def display_routes_to_destination(self, destination):
        match_routes = [route for route in self.routes if route.end_point == destination]
        if match_routes:
            for route in match_routes:
                print(f"Route Number: {route.route_number}, Route Name: {route.route_name}, Start Point: {route.start_point}, "+
                    f"Finish Point: {route.end_point}, Distance: {route.distance}") 
        else:
            print(f"No such route found: {destination}")

    def display_ticket_to_route_and_bus(self, bus_number, destination):
        match_route_ids = [route.route_id for route in self.routes if route.end_point == destination]

        match_bus_ids = [bus.bus_id for bus in self.buses if bus.bus_number == bus_number]
        
        match_tickets = [ticket for ticket in self.tickets if ticket.route_id in match_route_ids and ticket.bus_id in match_bus_ids]

        if match_tickets:
            for ticket in match_tickets:
                print(f"Passenger: {ticket.passenger_name}, Platform: {ticket.platform}, Seat: {ticket.seat_number}, "+
                    f"Destination: {ticket.destination}, Time: {ticket.departure_time}")
        else:
            print("Tickets to bus and routes: no found")
               

        