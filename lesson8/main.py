from controller import Controller

controller = Controller()

print('Routes list: ')
controller.display_routes()

print ('Routes list to destination: ')
controller.display_routes_to_destination("Lviv") #маршрут відсутній
controller.display_routes_to_destination("Sumy")

print ('Tickets to bus and routes: ')
controller.display_ticket_to_route_and_bus("K01", "Sumy")
controller.display_ticket_to_route_and_bus("K06", "Lviv") #білети на автобус відсутні
