class Route:
    def __init__(self, route_id, route_number, route_name, start_point, end_point, distance):
        self.route_id = route_id
        self.route_number = route_number
        self.route_name = route_name
        self.start_point = start_point
        self.end_point = end_point
        self.distance = distance