class Ticket:
    def __init__(self, route_id, bus_id, passenger_name, platform, seat_number, departure_time, destination):
        self.route_id = route_id
        self.bus_id = bus_id
        self.passenger_name = passenger_name
        self.platform = platform
        self.seat_number = seat_number
        self.departure_time = departure_time
        self.destination = destination