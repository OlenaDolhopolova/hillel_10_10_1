import math
import sys

def func(x):
    if not isinstance(x, (int, float)) :
        raise TypeError("Argument must be number")
    if x <= -10:   
        return -5
    if x >= 10:
        try:
            result = math.exp(x/5)
        except OverflowError:
            result = math.inf
        return result
    
    if x == 0:
        return 2
    else:
        return 1/x
    
def test_1():
    print('test first range min value')
    x = -sys.maxsize - 1
    result = func(x)
    assert result == -5
    print('ok')

def test_2():
    print('test first range max value')
    x = -10.01
    result = func(x)
    assert result == -5
    print('ok')

def test_3():
    print('test second range min value')
    x = -9.99
    result = func(x)
    assert result == 1/x
    print('ok')

def test_4():
    print('test second range max value')
    x = 9.99
    result = func(x)
    assert result == 1/x
    print('ok')

def test_5():
    print('test third range min value')
    x = 10.01
    result = func(x)
    assert result == math.exp(x/5)
    print('ok')

def test_6():
    print('test third range max value')
    x = 11.0
    result = func(x)
    assert result == math.inf or result == math.exp(x/5)
    print('ok')    

def test_7():
    print('test fourth range zero value')
    result = func(0)
    assert result == 2
    print('ok')


def test_8(x):
    print('test data type')
    try:
        func(x)
    except TypeError as e:
        print(e)
    print('ok')

if __name__ == '__main__':
    test_1()
    test_2()
    test_3()
    test_4()
    test_5()
    test_6()
    test_7()
    test_8(1)
    test_8(1.0)
    test_8('string')
    